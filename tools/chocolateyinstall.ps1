$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$filefullpath  = "$toolsDir\vnc\winvnc.exe"
$packageArgs = @{
  packageName   = 'uvnc'
  unzipLocation = $toolsDir
  filefullpath  = $filefullpath
  url           = 'https://gitlab.com/choco7/uvnc/-/raw/main/uvnc_x86.zip'
  url64bit      = 'https://gitlab.com/choco7/uvnc/-/raw/main/uvnc.zip'

  softwareName  = 'uvnc'

  checksum      = 'BAEB6B1A56466520925281F01AD2003F8E7ECA8D09A85287C95C474CFA288A0A'
  checksumType  = 'sha256'
  checksum64    = 'BA6128FE7F1AAB1BA31F7185D5AD0DCEE1DB04C615D19E7881C36FE0F54F2A56'
  checksumType64= 'sha256'

  validExitCodes= @(0)
  silentArgs   = ''
}

Install-ChocolateyZipPackage @packageArgs